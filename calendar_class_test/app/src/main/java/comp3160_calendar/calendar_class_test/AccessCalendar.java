package comp3160_calendar.calendar_class_test;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class AccessCalendar {
    static int _ID        = 0;
    static int IS_PRIMARY = 7;

    public static int displayAllCalendars(Context context) {

        Uri calendarUri = CalendarContract.Calendars.CONTENT_URI;
        String[] projection = new String[]{
                CalendarContract.Calendars._ID,
                CalendarContract.Calendars.ACCOUNT_NAME,
                CalendarContract.Calendars.ACCOUNT_TYPE,
                CalendarContract.Calendars.OWNER_ACCOUNT,
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
                CalendarContract.Calendars.NAME,
                CalendarContract.Calendars.CALENDAR_COLOR,
                CalendarContract.Calendars.IS_PRIMARY,
                CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL
        };

        Cursor Calcursor = context.getContentResolver().query(calendarUri, projection, null, null, null);
        int PrimarycalID = displaylistofCalendars(Calcursor);
        return PrimarycalID;
    }

    public static int addEventtoCalendar (
            Context context, long DTSTART, long DTEND, String EVENT_TIMEZONE, String TITLE, String DESCRIPTION,
            String EVENT_LOCATION, int HAS_ALARM, int CALENDAR_ID){

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();


        values.put(CalendarContract.Events.DTSTART, DTSTART);
        values.put(CalendarContract.Events.DTEND, DTEND);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, EVENT_TIMEZONE);
        values.put(CalendarContract.Events.TITLE, TITLE);
        values.put(CalendarContract.Events.DESCRIPTION, DESCRIPTION);
        values.put(CalendarContract.Events.EVENT_LOCATION, EVENT_LOCATION);
        values.put(CalendarContract.Events.HAS_ALARM, HAS_ALARM);
        values.put(CalendarContract.Events.CALENDAR_ID, CALENDAR_ID);
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        int id = Integer.parseInt(uri.getLastPathSegment());
        Log.d("TAG", "Created Calendar Event - " + id);

        return id;
    }

    public static int addAlarmtoEvent(Context context, int EVENT_ID, int MINUTES){

        ContentResolver cr = context.getContentResolver();
        ContentValues reminders = new ContentValues();
        reminders.put(CalendarContract.Reminders.EVENT_ID, EVENT_ID);
        reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        reminders.put(CalendarContract.Reminders.MINUTES, MINUTES);
        Uri urialert = cr.insert(CalendarContract.Reminders.CONTENT_URI, reminders);

        int arertid = Integer.parseInt(urialert.getLastPathSegment());
        Log.d("TAG", "Reminder have been saved successfully - " + arertid);

        return arertid;

    }

    public static int updateCalendarEntry(Context context, long DTSTART, long DTEND,
                                          int EVENT_ID, String TITLE, String DESCRIPTION,
                                          String EVENT_LOCATION, int HAS_ALARM, int CALENDAR_ID){

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();

        values.put(CalendarContract.Events.DTSTART, DTSTART);
        values.put(CalendarContract.Events.DTEND, DTEND);
        values.put(CalendarContract.Events.TITLE, TITLE);
        values.put(CalendarContract.Events.DESCRIPTION, DESCRIPTION);
        values.put(CalendarContract.Events.EVENT_LOCATION, EVENT_LOCATION);
        values.put(CalendarContract.Events.HAS_ALARM, HAS_ALARM);
        values.put(CalendarContract.Events.CALENDAR_ID, CALENDAR_ID);
        String where = "_id="+ EVENT_ID + " and " + CALENDAR_ID +
                "=" + CALENDAR_ID;
        int rowsUpdated = cr.update(CalendarContract.Events.CONTENT_URI, values, where, null);
        Log.d("TAG", "Updated event " + EVENT_ID + " in " + CALENDAR_ID);

        return rowsUpdated;
    }

    public static int deleteCalendarEntry(Context context, int EVENT_ID){

        ContentResolver cr = context.getContentResolver();

        Uri delete = null;
        delete = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, EVENT_ID);
        int rows = cr.delete(delete, null, null);
        Log.d("TAG", "Deleted " + rows + " event.");

        return rows;
    }

    private static int displaylistofCalendars(Cursor cursor) {
        int PrimarycalID = 0;
        cursor.moveToFirst();
        if (cursor != null) {
            do {
                /*if (cursor.getInt(IS_PRIMARY) == 1){
                    PrimarycalID = cursor.getInt(_ID);
                }    */
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    if(cursor.getColumnName(i).equals("dtstart")||cursor.getColumnName(i).equals("dtend")){
                        try{
                            long minutes = Long.parseLong(cursor.getString(i));
                            minutes = ((minutes/(1000*60))%60);
                            long hours = Long.parseLong(cursor.getString(i));
                            hours = ((hours/(1000*60*60))%24);
                            String info = hours + ":" + minutes;
                            Log.d("CALENDAR", " -> " + cursor.getColumnName(i)+ " = " + info);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    } else {
                        String info = cursor.getString(i);
                        Log.d("CALENDAR", " -> " + cursor.getColumnName(i)+ " = " + info);
                    }
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return PrimarycalID;
    }

   public static int displayAllEvents(Context context, int CAL_ID) {
       Uri EventUri = CalendarContract.Events.CONTENT_URI;
       String selection = "calendar_id="+CAL_ID;

       String[] projection = new String[]{
               CalendarContract.Events._ID,
               CalendarContract.Events.DTSTART,
               CalendarContract.Events.DTEND,
               CalendarContract.Events.TITLE,
               CalendarContract.Events.DESCRIPTION,
               CalendarContract.Events.EVENT_LOCATION,
               CalendarContract.Events.CALENDAR_ID};

       Cursor Eventcursor = context.getContentResolver().query(EventUri, projection, selection, null, null);
       int PrimarycalID = displaylistofCalendars(Eventcursor);
       return PrimarycalID;
   }
}