package comp3160_calendar.calendar_class_test;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {

    Button addButton, deleteButton, queryButton, displayButton, updateButton;
    TextView displayField;

    int CAL_ID = -1;
    Context context;
    int EVENT_ID = -1;
    int ALERT_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        addButton = (Button) findViewById(R.id.add_button);
        deleteButton = (Button) findViewById(R.id.delete_button);
        queryButton = (Button) findViewById(R.id.query_button);
        displayButton = (Button) findViewById(R.id.display_button);
        updateButton = (Button) findViewById(R.id.update_button);
        displayField = (TextView) findViewById(R.id.display_field);

        displayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CAL_ID = AccessCalendar.displayAllCalendars(context) + 1;
                Log.d("MAIN", "Primary ID: " + CAL_ID);
                displayField.setText("Main calendar ID = " + CAL_ID);
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long DTSTART = 0;
                long DTEND = 0;
                Calendar beginTime = Calendar.getInstance();
                beginTime.set(2016, 0, 25, 11, 10);
                DTSTART = beginTime.getTimeInMillis();
                Calendar endTime = Calendar.getInstance();
                endTime.set(2016, 0, 25, 11, 20);
                DTEND = endTime.getTimeInMillis();
                TimeZone timeZone = TimeZone.getDefault();
                String EVENT_TIMEZONE = timeZone.getID();
                String TITLE = "CALENDAR TEST";
                String DESCRIPTION = "Testing Content Provider in calendar context.";
                String EVENT_LOCATION = "Places.";
                int HAS_ALARM = 1;
                int MINUTES = 5;
                if (CAL_ID != -1){
                    EVENT_ID = AccessCalendar.addEventtoCalendar(context, DTSTART, DTEND, EVENT_TIMEZONE, TITLE, DESCRIPTION, EVENT_LOCATION, HAS_ALARM, CAL_ID);
                    ALERT_ID = AccessCalendar.addAlarmtoEvent(context,EVENT_ID, MINUTES);
                    displayField.setText("Event " + EVENT_ID + " added to calendar.");
                } else {
                    displayField.setText("Please press display calendars so the app can find the main one.");
                }
            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long DTSTART = 0;
                long DTEND = 0;
                Calendar beginTime = Calendar.getInstance();
                beginTime.set(2016, 0, 26, 11, 10);
                DTSTART = beginTime.getTimeInMillis();
                Calendar endTime = Calendar.getInstance();
                endTime.set(2016, 0, 26, 11, 20);
                DTEND = endTime.getTimeInMillis();
                TimeZone timeZone = TimeZone.getDefault();
                String EVENT_TIMEZONE = timeZone.getID();
                String TITLE = "UPDATE CALENDAR TEST";
                String DESCRIPTION = "Testing update on event " + EVENT_ID +", new message!";
                String EVENT_LOCATION = "Other places.";
                int HAS_ALARM = 1;
                if(EVENT_ID != -1){
                    int updateRows = AccessCalendar.updateCalendarEntry(context, DTSTART, DTEND, EVENT_ID, TITLE, DESCRIPTION, EVENT_LOCATION, HAS_ALARM, CAL_ID);
                    if(updateRows == 1){
                        displayField.setText("Event " + EVENT_ID + " updated.");
                    } else {
                        displayField.setText("Update failed.");
                    }
                } else {
                    displayField.setText("Please press add event so there is an event to update.");
                }
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(EVENT_ID != -1){
                    int deleted = AccessCalendar.deleteCalendarEntry(context, EVENT_ID);
                    if(deleted == 1){
                        displayField.setText("Event " + EVENT_ID + " deleted. Calendar needs to sync for it to show up in query.");
                        EVENT_ID = -1;
                    } else {
                        displayField.setText("Delete failed.");
                    }
                } else {
                    displayField.setText("Please add an event, can't delete nothing.");
                }
            }
        });

        queryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CAL_ID != -1){
                    int temp = AccessCalendar.displayAllEvents(context, CAL_ID);
                    displayField.setText("Check log for event details.");
                } else {
                    displayField.setText("Please press display calendars so the app can find the main one.");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
